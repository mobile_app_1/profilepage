import 'package:flutter/material.dart';

enum APP_THEME{LIGHT , DARK}

void main() {
  runApp(ContactProfilePage());
}

class MyAppTheme {
  static ThemeData appThemeLight() {
    return ThemeData(
      brightness: Brightness.light,
      appBarTheme: AppBarTheme(
        color: Colors.white,
        iconTheme: IconThemeData(
          color: Colors.black,
        ),
      ),
      iconTheme: IconThemeData(
        color: Colors.pink,
      ),
    );
  }

  static ThemeData appThemeDark() {
    return ThemeData(
      brightness: Brightness.dark,
      appBarTheme: AppBarTheme(
        color: Colors.white,
        iconTheme: IconThemeData(
          color: Colors.white,
        ),
      ),
      iconTheme: IconThemeData(
        color: Colors.pink,
      ),
    );
  }
}

class ContactProfilePage extends StatefulWidget {
  const ContactProfilePage({super.key});
  @override
  State<ContactProfilePage> createState() => _ContactProfilePageState();

}

class _ContactProfilePageState extends State<ContactProfilePage> {
  var currentTheme = APP_THEME.LIGHT;
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      theme: currentTheme == APP_THEME.DARK
      ? MyAppTheme.appThemeLight()
      : MyAppTheme.appThemeDark(),

      home: Scaffold(
        appBar: buildAppBarWidget(),
        body: buildBodyWidget(),
        floatingActionButton: FloatingActionButton(
          child: Icon(Icons.toggle_off),
          onPressed: () {
            setState(() {
              currentTheme == APP_THEME.DARK
                  ? currentTheme = APP_THEME.LIGHT
                  : currentTheme = APP_THEME.DARK;
            });
          },
        ),
      ),
    );
  }
}
AppBar buildAppBarWidget() {
  return AppBar(
    backgroundColor: Colors.pinkAccent,
    leading: Icon(
      Icons.arrow_back,
      color: Colors.black,
    ),
    actions: <Widget>[
      IconButton(
          onPressed: () {print("Contact is starred");},
          icon: Icon(Icons.star_border),
          color: Colors.black),
    ],
  );
}

Widget buildBodyWidget() {
  return ListView(
    children: <Widget>[
      Column(
        children: <Widget>[
          Container(width: double.infinity,

            //Height constraint at Container widget level
            height: 280,//250
            child: Image.network(
              "https://scontent.fbkk10-1.fna.fbcdn.net/v/t1.6435-9/40542970_1487603241341427_1906717122940108800_n.jpg?_nc_cat=108&ccb=1-7&_nc_sid=174925&_nc_ohc=9rqd09azBL4AX_RVWqk&_nc_ht=scontent.fbkk10-1.fna&oh=00_AfD2_KhGRT1w4CX-hyG48UgEcKNafRaojm4jGta6iP0vzA&oe=63C9F6A8",
              fit: BoxFit.contain,
            ),
          ),
          Container(
            height: 60,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.start,

              children: <Widget>[
                Padding(
                  padding: EdgeInsets.all(8.0),
                  child :Text("screamprincessb",
                    style: TextStyle(fontSize: 25),
                  ),
                ),
              ],
            ),
          ),
          Divider(
            color: Colors.grey,
          ),

           Container(
             margin: const EdgeInsets.only(top: 8, bottom: 8),
             child: Theme (
               data: ThemeData (
                 iconTheme: IconThemeData (
                   color: Colors.blueAccent,
                 ),
               ),
               child: profileActionsItems() ,
            ),
           ),
          Divider(
            color: Colors.black,
          ),
          mobilePhoneListTile(),
          otherPhoneListTile(),

          Divider(
            color: Colors.grey,
          ),

          emailListTile(),
          Divider(
            color: Colors.grey,
          ),

          addressListTile(),
          Divider(
            color: Colors.grey,
          ),
        ],
      ),
    ],
  );
}

Widget buildCallButton() {
  return Column(
    children: <Widget>[
      IconButton(
        icon: Icon(
          Icons.call,
          //color: Colors.pink,
        ),
        onPressed: () {},
      ),
      Text("Call"),
    ],
  );
}

Widget buildTextButton() {
  return Column(
    children: <Widget>[
      IconButton(
        icon: Icon(
          Icons.message,
          //color: Colors.pink,
        ),
        onPressed: () {},
      ),
      Text("Text"),
    ],
  );
}

Widget buildVideoCallButton() {
  return Column(
    children: <Widget>[
      IconButton(
        icon: Icon(
          Icons.video_call,
          //color: Colors.pink,
        ),
        onPressed: () {},
      ),
      Text("video"),
    ],
  );
}

Widget buildEmailButton() {
  return Column(
    children: <Widget>[
      IconButton(
        icon: Icon(
          Icons.email,
          //color: Colors.pink,
        ),
        onPressed: () {},
      ),
      Text("Email"),
    ],
  );
}

Widget buildDirectionsButton() {
  return Column(
    children: <Widget>[
      IconButton(
        icon: Icon(
          Icons.directions,
          //color: Colors.pink,
        ),
        onPressed: () {},
      ),
      Text("Directions"),
    ],
  );
}

Widget buildPayButton() {
  return Column(
    children: <Widget>[
      IconButton(
        icon: Icon(
          Icons.attach_money,
          //color: Colors.pink,
        ),
        onPressed: () {},
      ),
      Text("pay"),
    ],
  );
}

//ListTile
Widget mobilePhoneListTile() {
  return ListTile(
    leading: Icon(Icons.call) ,
    title: Text("330-803-3398"),
    subtitle: Text("mobile"),
    trailing: IconButton(
      icon: Icon(Icons.message) ,
      color: Colors.pink ,
      onPressed: () {},
    ),
  );
}

Widget otherPhoneListTile() {
  return ListTile(
    leading: Text(""),
    title: Text("440-440-4400"),
    subtitle: Text("other"),
    trailing: IconButton(
      icon: Icon(Icons.message) ,
      color: Colors.pink ,
      onPressed: () {},
    ),
  );
}

Widget emailListTile() {
  return ListTile(
    leading: Icon(Icons.mail) ,
    title: Text("screamprincessb@gmail.com"),
    subtitle: Text("work"),
  );
}

Widget addressListTile() {
  return ListTile(
    leading: Icon(Icons.location_on) ,
    title: Text("123 Bangsaen Chonburi , Thailand"),
    subtitle: Text("home"),
    trailing: IconButton(
      icon: Icon(Icons.directions) ,
      color: Colors.pink ,
      onPressed: () {},
    ),
  );
}

Widget profileActionsItems() {
  return Row(
      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
  children: <Widget>[
  buildCallButton(),
  buildTextButton(),
  buildVideoCallButton(),
  buildEmailButton(),
  buildDirectionsButton(),
  buildPayButton(),
  ],
  );
}